#pragma once

// For use by Propinquity Applications

#include "Propinquity/Application.h"
#include "Propinquity/Log.h"

// -----Entry Point-------------------
#include "Propinquity/EntryPoint.h"
// -----------------------------------