#pragma once

#include <memory>

#include "Core.h"
#include "spdlog/spdlog.h"

namespace Propinquity
{

	class PROPINQUITY_API Log
	{
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};

}

// Core log macros
#define PQ_CORE_TRACE(...)   ::Propinquity::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define PQ_CORE_INFO(...)    ::Propinquity::Log::GetCoreLogger()->info(__VA_ARGS__)
#define PQ_CORE_WARN(...)    ::Propinquity::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define PQ_CORE_ERROR(...)   ::Propinquity::Log::GetCoreLogger()->error(__VA_ARGS__)
#define PQ_CORE_FATAL(...)   ::Propinquity::Log::GetCoreLogger()->fatal(__VA_ARGS__)

// Client log macros
#define PQ_TRACE(...)        ::Propinquity::Log::GetClientLogger()->trace(__VA_ARGS__)
#define PQ_INFO(...)         ::Propinquity::Log::GetClientLogger()->info(__VA_ARGS__)
#define PQ_WARN(...)         ::Propinquity::Log::GetClientLogger()->warn(__VA_ARGS__)
#define PQ_ERROR(...)        ::Propinquity::Log::GetClientLogger()->error(__VA_ARGS__)
#define PQ_FATAL(...)        ::Propinquity::Log::GetClientLogger()->fatal(__VA_ARGS__)