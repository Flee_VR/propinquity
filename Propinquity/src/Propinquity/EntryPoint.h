#pragma once


#ifdef PQ_PLATFORM_WINDOWS

extern Propinquity::Application* Propinquity::CreateApplication();

int main(int argc, char** argv)
{
	Propinquity::Log::Init();
	PQ_CORE_WARN("Initialized Log!");
	int a = 20;
	PQ_INFO("Hello! Var={0}", a);

	auto app = Propinquity::CreateApplication();
	app->Run();
	delete app;
}

#endif