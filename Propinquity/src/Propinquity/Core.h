#pragma once

#ifdef PQ_PLATFORM_WINDOWS
	#ifdef PQ_BUILD_DLL
		#define PROPINQUITY_API __declspec(dllexport)
	#else
		#define PROPINQUITY_API __declspec(dllimport)
	#endif
#else
	#error Propinquity only supports Windows!
#endif