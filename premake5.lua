workspace "Propinquity"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "Propinquity"
	location "Propinquity"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"%{prj.name}/vendor/spdlog/include"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"PQ_PLATFORM_WINDOWS",
			"PQ_BUILD_DLL"
		}

		postbuildcommands
		{
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
		}

	filter "configurations:Debug"
		defines "PQ_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "PQ_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "PQ_DIST"
		optimize "On"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"Propinquity/vendor/spdlog/include",
		"Propinquity/src"
	}

	links
	{
		"Propinquity"
	}

	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines
		{
			"PQ_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "PQ_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "PQ_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "PQ_DIST"
		optimize "On"