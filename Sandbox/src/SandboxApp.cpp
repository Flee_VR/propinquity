#include <Propinquity.h>

class Sandbox : public Propinquity::Application
{
public:
	Sandbox() {

	}

	~Sandbox()
	{

	}
};

Propinquity::Application* Propinquity::CreateApplication()
{
	return new Sandbox();
}